from __future__ import print_function
import numpy as np
import psi4

def get_DF_coeff(orb,aux,D):
    """Calculates density fitting coefficients in aux basis given original orb basis and density matrix D."""

    zero_bas = psi4.core.BasisSet.zero_ao_basis_set()

    # Build MintsHelper Instance
    mints = psi4.core.MintsHelper(orb)

    # Build (ab|Q) raw 3-index ERIs, dimension (nbf, nbf, nAux, 1)
    #Ppq = mints.ao_eri(zero_bas, aux, orb, orb)
    abQ = mints.ao_eri(orb, orb, aux, zero_bas)

    # Build and invert the coulomb metric
    Jinv = mints.ao_eri(zero_bas, aux, zero_bas, aux)
    Jinv.power(-1.0, 1.e-14)

    # Remove the excess dimensions of abQ & metric
    abQ = np.squeeze(abQ)
    Jinv = np.squeeze(Jinv)

    # Contract abQ & Jinv to build dab_P
    dab_P = np.einsum('abQ,QP->abP', abQ, Jinv, optimize=True)
    C_P = np.einsum('abP,ab->P', dab_P, D, optimize=True)

    return C_P

def calc_powerspec(aux,C_P):
    """Calculates powerspectrum given aux basis, original orb basis and density matrix D. """
    shells = []
    shells_to_at = []
    preshell = -1
    currshell = []
    for i_bf in range(aux.nbf()):
        #print('bf ',i_bf,' on atom ',aux.function_to_center(i_bf),' shell ',aux.function_to_shell(i_bf))
        f2s = aux.function_to_shell(i_bf)
        f2c = int(aux.function_to_center(i_bf))
        if f2s == preshell:
            currshell.append(i_bf)
        else:
            shells.append(currshell)
            currshell = [i_bf]
            preshell  = f2s
            shells_to_at.append(f2c)
    shells.append(currshell)

    #print(shells[1:])
    #print(shells_to_at)
    powerspec = []
    preat = -1
    currat = []
    for i_s,shell in enumerate(shells[1:]):
        p = np.sum(C_P[shell]**2)
        #print('shell ',i_s,' on atom ', shells_to_at[i_s],' p= ',p)
        if shells_to_at[i_s] == preat:
            currat.append(p)
        else:
            powerspec.append(currat)
            currat = [p]
            preat  = shells_to_at[i_s]
    powerspec.append(currat)
    return powerspec[1:]

if __name__ == "__main__":

    # Set numpy defaults
    np.set_printoptions(precision=5, linewidth=200, suppress=True)

    # Set Psi4 memory & output options
    psi4.set_memory(int(2e9))
    psi4.core.set_output_file('output.dat', False)

    # Geometry specification
    mol = psi4.geometry("""
    O
    H 1 0.96
    H 1 0.96 2 104.5
    symmetry c1
    """)

    basis = 'def2-SVP'

    # Psi4 options
    psi4.set_options({'basis': basis,
                      'scf_type': 'df',
                      'e_convergence': 1e-10,
                      'd_convergence': 1e-10})

    wfn = psi4.core.Wavefunction.build(mol, psi4.core.get_global_option('basis'))

    scf_e, scf_wfn = psi4.energy('scf', return_wfn=True)
    D = scf_wfn.Da()

    # Build auxiliary basis set
    aux = psi4.core.BasisSet.build(mol, "DF_BASIS_SCF", "", "JKFIT", basis)
    orb = wfn.basisset()
    zero_bas = psi4.core.BasisSet.zero_ao_basis_set()

    # Get Density Fitting Coefficients
    C_P = get_DF_coeff(orb,aux,D)

    # Get Powerspectrum
    ps = calc_powerspec(aux,C_P)
    print(ps[0])
    print(ps[1])
    print(ps[2])
