import numpy as np
import sys
sys.path.append('../../')

from kdf import train_model
from kdf_helper import learning_curve


dirname = '../data/H2O_dimer/'

train_sizes = [10,20,50,100]
test_errors = []

for n_train in train_sizes:
    res,y_test,y_ref,E_HF = train_model(dirname=dirname,Ntrain=n_train,trajname='refdata.xyz',Nval=100,Ntest=200,Nmax=300,doCV=False)
    test_errors.append(res['mae_test'])

learning_curve(train_sizes,test_errors,filename='learning_curve.png')

