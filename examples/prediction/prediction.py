import numpy as np
import sys
sys.path.append('../../')

from kdf import predict_mols
from ase.io import *


mols = read('predictdata.xyz@::')

y_pred,E_HF = predict_mols(mols,dirname='../data/H2O_dimer/')

print(y_pred)
#np.savetxt('E_cc_predict.txt',y_pred)
#np.savetxt('E_HF_predict.txt',E_HF)

