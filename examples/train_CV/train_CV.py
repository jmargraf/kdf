import numpy as np
import sys
sys.path.append('../../')

from kdf import train_model
from kdf_helper import correlation_plot


dirname = '../data/H2O_dimer/'
n_train = 100

res,y_test,y_ref,E_HF = train_model(dirname=dirname,Ntrain=n_train,trajname='refdata.xyz',Nval=100,Ntest=200,Nmax=300,doCV=True)
correlation_plot(y_ref,y_test,alpha=0.5,bounds='kT',filename='train_CV.png')
print(res)


