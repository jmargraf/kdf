from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def power_law(x, a, b) :
    return a * np.power(x, b)

def linear_function(x, a, b) :
    return a + x * b

def curve_fit_log(x, y) :
    """Fits linear function in log-log scale, returns function in linear scale"""
    
    logx = np.log10(x)
    logy = np.log10(y)
    
    # do linear fit
    popt_log, pcov_log = curve_fit(linear_function, logx, logy)
    
    # back transform fitted function
    yfit = np.power(10, linear_function(logx, *popt_log))

    return yfit

def learning_curve(train_sizes,test_errors,filename=None,dpi=300):
    nmin = train_sizes[0]-1
    nmax = train_sizes[-1]+10
    plt.plot(np.linspace(nmin,nmax,10),[0.043364]*10,c='k',linestyle='--',alpha=0.5)
    plt.plot(np.linspace(nmin,nmax,10),[0.02585184]*10,c='k',linestyle='-.',alpha=0.5)
    
    plt.scatter(train_sizes,test_errors)
    yfit = curve_fit_log(train_sizes,test_errors)
    plt.plot(train_sizes,yfit,linestyle=":")

    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('$N_{\mathrm{train}}$',fontsize=16)
    plt.ylabel('MAE / eV',fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=14)

    plt.xlim([nmin,nmax])

    if filename == None:
        plt.show()
    else:
        plt.savefig(filename,bbox_inches='tight',dpi=dpi)

def correlation_plot(y_ref,y_test,alpha=0.5,bounds='kT',filename=None,dpi=300):
    plt.scatter(y_ref,y_test,alpha=alpha)
    alldat = np.hstack([y_ref,y_test])

    kcalmol = 0.043364115
    kT = 0.0258518428
    plt.plot(np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),c='k')
    if bounds=='kT':
        plt.plot(np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50)+kT,c='k',linestyle=':')
        plt.plot(np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50)-kT,c='k',linestyle=':')
    elif bounds=='kcalmol':
        plt.plot(np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50)+kcalmol,c='k',linestyle='--')
        plt.plot(np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50),np.linspace(np.min(alldat)-0.1,np.max(alldat)+0.1,50)-kcalmol,c='k',linestyle='--')
        
        
    plt.xlabel('$E_{\mathrm{C}}^{\mathrm{ref}}$ / eV',fontsize=16)
    plt.ylabel('$E_{\mathrm{C}}^{\mathrm{ML}}$ / eV',fontsize=16)
    plt.tick_params(axis='both', which='major', labelsize=14)

    if filename == None:
        plt.show()
    else:
        plt.savefig(filename,bbox_inches='tight',dpi=dpi)

if __name__ == "__main__":
    print("import me!")
