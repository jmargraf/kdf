# Using the KernelDensityFunctionalTheory (KDFT) Code 

## Dependencies

For full functionality you will need Python 3.7 (not tested with lower versions) including `numpy`, `matplotlib` and `scipy`. Further dependencies are `ase` and `Psi4`. I recommend setting up a conda environment for this purpose (that's definitely the easiest way to get Psi4).

## Structure

The KDFT code consists of three modules:

`kdf` contains the core functionality for fitting KDF models and doing predictions.

`kdf_helper` contains some helper functions for plotting and analysis.

`ridr` contains the functionality for generating the rotationally invariant density representation via density fitting.

The folder `examples` contains scripts for running typical tasks and some test data for the water dimer.

Below are some quick explanations for each example..

## Fitting a KDFT model using a validation set

The cell below shows how to fit a model using precomputed densities stored in `examples/data/H2O_dimer/`. We specify 100 training points (this will be the first 100 configurations in the file `refdata.xyz`). The notation for test and validation sets are a little idiosynchratic: Here we use `Nval=100` and `Ntest=200`. This means that the validation set will go from index 100 to 199 and the test set from 200 to `Nmax`. If `Nmax` is not specified, all remaining configurations are used, starting at `Ntest`. The function `train_model` returns the predicted and reference data for the test set, along with some statistics for test and validation set (as well as the optimal hyperparameter `sigma`).


```python
from kdf import train_model
from kdf_helper import correlation_plot


dirname = '../data/H2O_dimer/'
n_train = 100

res,y_test,y_ref,E_HF = train_model(dirname=dirname,Ntrain=n_train,trajname='refdata.xyz',Nval=100,Ntest=200,Nmax=300)
correlation_plot(y_ref,y_test,alpha=0.5,bounds='kT')
print(res)

```


![png](examples/train_noCV/train_noCV.png)


    {'sigma': 3.1078661877820138e-09, 'mae_val': 0.009113551594764431, 'mae_test': 0.009501680260118519, 'rmse_val': 0.014060829351454949, 'rmse_test': 0.016779950773846943}


## Fitting a KDFT model using cross-validation

Here, the same fit is repeated using ten-fold cross-validation on the training set instead of a separate validation set (`doCV=True`). In this case, `Nval` need not be specified.


```python
from kdf import train_model
from kdf_helper import correlation_plot


dirname = '../data/H2O_dimer/'
n_train = 100

res,y_test,y_ref,E_HF = train_model(dirname=dirname,Ntrain=n_train,trajname='refdata.xyz',Nval=100,Ntest=200,Nmax=300,doCV=True)
correlation_plot(y_ref,y_test,alpha=0.5,bounds='kT')
print(res)

```


![png](examples/train_CV/train_CV.png)


    {'sigma': 1e-09, 'mae_val': 0.009603512341454916, 'mae_test': 0.012318547584281987, 'rmse_val': 0.01991269445784454, 'rmse_test': 0.0340532893837049}


## Learning curves

A common task when building new models is the generation of a learning curve, to see how the test error behaves as more training data is added:


```python
from kdf import train_model
from kdf_helper import learning_curve


dirname = '../data/H2O_dimer/'

train_sizes = [10,20,50,100]
test_errors = []

for n_train in train_sizes:
    res,y_test,y_ref,E_HF = train_model(dirname=dirname,Ntrain=n_train,trajname='refdata.xyz',Nval=100,Ntest=200,Nmax=300,doCV=False)
    test_errors.append(res['mae_test'])

learning_curve(train_sizes,test_errors)

```


![png](examples/learning_curve/learning_curve.png)


# Reusing data

The training procedure produces a number of new files. 

Using Psi4, two files are generated in the reference directory `dirname`:

`E_HF.json`: Contains the Hartree Fock energies of all configurations and is only generated if not already in the directory.

`powerspectra.json`: Contains the corresponding powerspectra, which encode the electron density. It is also only generated if not already in directory.

From the training itself, two files are generated and saved in the current working directory:

`kernel.txt`: Contains the Kernel matrix and is only generated if it's not already in the current working directory.

`model.txt`: Contains the KRR coefficients needed to make predictions. This file is always generated, even if there already is such a file in the current working directory.

The example directory already contains the `.json` files, which makes the execution of these examples quite quick. If they are deleted, Psi4 will be required to run the underlying HF calculations.

# Prediction for unknown molecules

We can now use our pretrained model to predict the correlation energies of a new set of configurations. This can be done with the `predict_mols` function, which expects a list of ASE atoms objects as input. We also need to specify where the pretrained model can be found, i.e. the directory containing the `model.txt` and `powerspectra.json` files. The method returns predicted correlation and calculated HF energies. It also generates two new files, namely `kernel_transfer.txt` and `powerspectra_transfer.json`.


```python
from kdf import predict_mols
from ase.io import *


mols = read('predictdata.xyz@::')

y_pred,E_HF = predict_mols(mols,dirname='../data/H2O_dimer/')

print(y_pred)

```

