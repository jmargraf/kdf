from __future__ import print_function
import numpy as np
import json
import os
from ase.io import *
from ase.units import Hartree
import psi4
from ase.calculators.psi4 import Psi4
from ridr import *
from copy import deepcopy

def gen_ps(traj,Verbose=True,Basis='def2-TZVP',Qtot=0):
    """Generate new powerspectra using Psi4"""
    Ps = []
    Es = []

    hfstring = 'HF/' + Basis

    for imol,atoms in enumerate(traj):
        if Verbose:
            print('running HF calculation for molecule ',imol)
        psi4.core.clean()
        psi4.core.set_output_file('output.dat', False)

        calc = Psi4(atoms = atoms,
            charge = Qtot,
            multiplicity = 1,
            method = 'HF',
            memory = '2000MB',
            basis = Basis)

        e, wfn = calc.psi4.energy(hfstring, molecule=calc.molecule,
                        return_wfn=True)
        mol = calc.molecule
        D = wfn.Da()

        # Build auxiliary basis set
        aux = psi4.core.BasisSet.build(mol, "DF_BASIS_SCF", "", "JKFIT", Basis)
        orb = wfn.basisset()

        # Get Density Fitting Coefficients
        C_P = get_DF_coeff(orb,aux,D)

        # Get Powerspectrum
        ps = calc_powerspec(aux,C_P)
        Ps.append(ps)
        Es.append(e)

    os.remove("timer.dat")
    os.remove("output.dat")

    return Ps,np.array(Es)*Hartree


def rmse(V1,V2):
    """Calculate root means square deviation."""
    rmsd = 0.0
    for i,e1 in enumerate(V1):
        rmsd += (e1-V2[i])**2
    rmsd /= float(len(V1))
    return np.sqrt(rmsd)

def mae(V1,V2):
    """Calculate mean absolute error."""
    mae = 0.0
    diff = np.abs(V1-V2)
    return np.mean(diff)

def local_kernel(p1,p2,zeta=1.0):
    """Calculate kernel of two atomic densities."""
    if len(p1) != len(p2):
        return 0.0
    else:
        p1 = np.array(p1)
        p2 = np.array(p2)
        p12 = np.dot(p1,p2)**zeta
        p11 = np.dot(p1,p1)**zeta
        p22 = np.dot(p2,p2)**zeta
        return p12/(np.sqrt(p11*p22))

def sum_kernel(m1,m2,zeta=1.0):
    """Calculate the global sum kernel between two molecular densities."""
    kernel = 0.0
    for A in m1:
        for B in m2:
            kernel += local_kernel(A,B,zeta=zeta)
    return kernel

def train(K,Y,sig):
    """KRR training."""
    I = np.eye(K.shape[0])
    alpha = np.matmul(np.linalg.inv(K+sig*I),Y)
    #print("training done")
    return alpha

def predict(K,alpha):
    """KRR prediction."""
    pred = np.matmul(K,alpha)
    return pred

def cross_val(sig,K,Y,Nfold=10):
    """Simple cross-validation routine."""
    Ntot = K.shape[0]
    Nval = Ntot/Nfold
    maes = []

    for ifold in range(Nfold):
        idx_val = np.arange(ifold*Nval,(ifold+1)*Nval,dtype=np.int32)
        idx_all = np.arange(Ntot,dtype=np.int32)
        idx_train = idx_all[tuple([~np.isin(idx_all,idx_val)])]

        alpha   = train(K[np.ix_(idx_train,idx_train)],Y[idx_train],sig)
        y_val   = predict(K[np.ix_(idx_val,idx_train)],alpha)

        mae_val = mae(y_val,Y[idx_val])
        maes.append(mae_val)
    return(np.mean(np.array(maes)))

def predict_mols(mols,dirname='./',savekernel=False,Qtot=0,Basis='def2-TZVP'):
    """Predicts energies of unknown molecules with previously fitted ML-DFA."""
    with open((dirname+'powerspectra.json'),'r') as f:
        ps1 = json.load(f)

    alpha = np.loadtxt((dirname+'model.txt'))
    ps1 = ps1[:alpha.shape[0]]

    try:
        with open(('powerspectra_predict.json'),'r') as f:
            ps2 = json.load(f)

        with open(('E_HF_predict.json'),'r') as f:
            E_HF = json.load(f)
            E_HF = np.array(E_HF)
            E_HF *= Hartree
    except:
        ps2, E_HF = gen_ps(mols,Qtot=Qtot,Basis=Basis)
        with open(('powerspectra_predict.json'),'w') as f:
            json.dump(ps2,f)
        with open(('E_HF_predict.json'),'w') as f:
            E_HF_Ha = list(E_HF*(1./Hartree))
            json.dump(E_HF_Ha,f)

    Nmax = len(ps1)
    Nmax2 = len(ps2)

    K_pred = np.zeros((Nmax2,Nmax))
    for i in range(Nmax2):
        for j in range(Nmax):
            ker = sum_kernel(ps2[i],ps1[j],zeta=2.)
            K_pred[i,j] = ker
    if savekernel:
        np.savetxt(('kernel_predict.txt'),K_pred)

    Y_pred  = predict(K_pred,alpha)
    return Y_pred,E_HF

def train_model(dirname='./',Ntrain=100,Nval=800,Ntest=900,Nmax=None,trajname='ensemble.xyz',doCV=False,Qtot=0,Basis='def2-TZVP'):
    """Train ML-DFA model. Uses precomputed powerspectra and HF energies if available or recomputes with Psi4 otherwise."""
    if Nmax == None:
        traj = read((dirname+trajname+'@:'))
        Nmax = len(traj)
    else:
        traj = read((dirname+trajname+'@:'+str(Nmax)))

    try:
        with open((dirname+'powerspectra.json'),'r') as f:
            ps = json.load(f)

        with open((dirname+'E_HF.json'),'r') as f:
            E_HF = json.load(f)
            E_HF = np.array(E_HF)
            E_HF *= Hartree
    except:
        ps, E_HF = gen_ps(deepcopy(traj),Qtot=Qtot,Basis=Basis)
        with open((dirname+'powerspectra.json'),'w') as f:
            json.dump(ps,f)
        with open((dirname+'E_HF.json'),'w') as f:
            E_HF_Ha = list(E_HF*(1./Hartree))
            json.dump(E_HF_Ha,f)

    E_CC = np.array([mol.get_potential_energy() for mol in traj])

    Y = E_CC - E_HF  

    try:
        K = np.loadtxt(('kernel.txt'))
    except:
        K = np.zeros((Nmax,Nmax))
        for i in range(Nmax):
            for j in range(i+1):
                ker = sum_kernel(ps[i],ps[j],zeta=2.)
                K[i,j] = ker
                K[j,i] = ker
        np.savetxt(('kernel.txt'),K)

    sigs    = np.logspace(-9,-2,200)
    maes    = []
    for sig in sigs:
        if doCV:
            mae_val = cross_val(sig,K[:Ntrain,:Ntrain],Y[:Ntrain],Nfold=10)
            maes.append(mae_val)
        else:
            #print("start training")
            alpha  = train(K[:Ntrain,:Ntrain],Y[:Ntrain],sig)
            #print("start predictions")
            y_val   = predict(K[Nval:Ntest,:Ntrain],alpha)
            y_test  = predict(K[Ntest:,:Ntrain],alpha)

            mae_val = mae(y_val,Y[Nval:Ntest])
            maes.append(mae_val)
            mae_tes = mae(y_test,Y[Ntest:Nmax])

    sig = sigs[maes.index(min(maes))]
    alpha  = train(K[:Ntrain,:Ntrain],Y[:Ntrain],sig)
    
    np.savetxt(('model.txt'),alpha)

    y_val   = predict(K[Nval:Ntest,:Ntrain],alpha)
    y_test  = predict(K[Ntest:,:Ntrain],alpha)
    mae_val = mae(y_val,Y[Nval:Ntest])
    mae_tes = mae(y_test,Y[Ntest:Nmax])
    rmse_val = rmse(y_val,Y[Nval:Ntest])
    rmse_tes = rmse(y_test,Y[Ntest:Nmax])
    res = {'sigma':sig,'mae_val':mae_val,'mae_test':mae_tes,'rmse_val':rmse_val,'rmse_test':rmse_tes}

    return res,y_test,Y[Ntest:],E_HF[Ntest:]

